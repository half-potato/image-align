import numpy as np
import sys, os, cv2
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from alignment import patches

w = 30
h = 30
img = np.random.rand(h, w)
cv2.imshow("Original", img)
grid_size = 15
tw, th = patches.get_bounds(img, grid_size)
for ix in range(tw):
    for iy in range(th):
        patch = patches.get_patch(img, ix, iy, grid_size, overlap=2)
        print(patch.shape)
        cv2.imshow("Patch", patch)
        cv2.waitKey(0)

