import numpy as np
import sys, os, cv2
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from alignment import imagealign, fft, imutils
# test: normal alignment (no fractional component)

w, h = 10, 10
im1 = np.random.rand(w, h)
im2 = np.random.rand(w, h)
gap = np.ones((h, 1), np.float)

possibleshifts = (-2, -1, 0, 1, 2)
upsampling = 0

def region(xmin, xmax, ymin, ymax):
    yslice = slice(np.clip(ymin, 0, None),np.clip(ymax, None, h))
    xslice = slice(np.clip(xmin, 0, None),np.clip(xmax, None, w))
    return yslice, xslice

im1_fft = fft.get_fft(im1, upsampling)
for xs in possibleshifts:
    for ys in possibleshifts:
        shifted_im = im2.copy()
        shifted_im[region(xs, xs+w, ys, ys+h)] = \
            im1[region(-xs, -xs+w, -ys, -ys+h)]
        sfft = fft.get_fft(shifted_im)
        x, y = imagealign.get_shift(im1_fft, sfft, upsampling)
        imaled = imutils.align(shifted_im, x, y)
        sidebyside1 = np.hstack((im1, gap, shifted_im))
        cv2.imshow("Original, shifted", sidebyside1)
        if chr(cv2.waitKey(0) & 0xFF) == "q":
            break

        crop_original = imutils.crop_offset(im1, x, y)
        crop_shifted = imutils.crop_offset(shifted_im, x, y)
        crop_aligned = imutils.crop_offset(imaled, x, y)

        error = np.mean((crop_original-crop_aligned)**2)
        print("Error: %f" % error)
        seperator = np.ones(
            (imutils.crop_offset(imaled, x, y).shape[0], 1), np.float)
        sidebyside2 = np.hstack((
            crop_original, seperator,
            crop_shifted, seperator,
            crop_aligned))
        cv2.imshow("Original, shifted, aligned", sidebyside2)
        if chr(cv2.waitKey(0) & 0xFF) == "q":
            break
