import numpy as np
import sys, os, cv2
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from alignment import imagealign

# test: linear interp

w, h = 10, 10
im = np.random.rand(w, h)
cv2.imshow("Original", im)
cv2.waitKey(0)
gap = np.ones((h, 1), np.float)

step = 0.2
possibleshifts = np.arange(-1, 1, step)

for xs in possibleshifts:
    for ys in possibleshifts:
        sim = imagealign.align(im, xs, ys)
        cv2.imshow("Shifted", sim)
        if chr(cv2.waitKey(0) & 0xFF) == "q":
            break
