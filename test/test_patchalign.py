import numpy as np
import sys, os, cv2
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from alignment import imagealign, fft, imutils, patches, interp
import noise
# test: fractional alignment
# Take a patch of the original image
# Shift it fractionally using linear interp
# Try and match it back to original image

cv2.namedWindow("R", cv2.WINDOW_NORMAL)
cv2.namedWindow("Patch", cv2.WINDOW_NORMAL)
cv2.namedWindow("Template", cv2.WINDOW_NORMAL)
cv2.namedWindow("Merged", cv2.WINDOW_NORMAL)
cv2.namedWindow("Blur", cv2.WINDOW_NORMAL)
w, h = 45*2, 45*2
#im = np.random.rand(w//2, h//2)
#im = cv2.pyrUp(im)
lin = np.linspace(0,10,w, endpoint=False)
x, y = np.meshgrid(lin, lin)
print(x,y)
im = noise.perlin(x, y)
gap = np.ones((h, 1), np.float) # gap between images for display

grid_size = 15
overlap = 0
tw, th = patches.get_bounds(im, grid_size)

step = 0.2
possibleshifts = np.arange(-1, 1, step)

def region(xmin, xmax, ymin, ymax):
    yslice = slice(np.clip(ymin, 0, None),np.clip(ymax, None, h))
    xslice = slice(np.clip(xmin, 0, None),np.clip(xmax, None, w))
    return yslice, xslice

cv2.imshow("Template", im)
im_fft = fft.get_fft(im)
#im_fft = np.fft.fft2(im)

for tx in range(tw):
    for ty in range(th):
        xshift, yshift = patches.get_patch_loc(im, tx, ty, grid_size, im.shape)
        print(xshift, yshift)
        # Generate shifted patch
        patch = patches.get_patch(im, tx, ty, grid_size)
        cv2.imshow("Patch", patch)
        sfft = fft.get_fft(patch)
        dx = im_fft.shape[0] - sfft.shape[0]
        dy = im_fft.shape[1] - sfft.shape[1]
        pad_patch = np.pad(patch, ((0,dx), (0, dy)), "constant", constant_values=(0))
        #sfft = np.fft.fft2(patch)
        pad_fft = np.pad(sfft, ((0,dx), (0, dy)), "constant", constant_values=(0))
        fft_of_padded = fft.get_fft(pad_patch)

        #x, y = imagealign.phase_correlation(im_fft, pad_fft)
        x, y = imagealign.phase_correlation2(im_fft, pad_fft)
        matching = patches.get_containing_patch(im, x, y, grid_size)
        cv2.imshow("Matching", matching)
        patches.merge_patch(im, patch, im.shape[1]/2, im.shape[0]/2)
        #x, y = imagealign.phase_correlation2(im, pad_patch)
        #print(x,y)

        cv2.waitKey(0)
        #a_patch = imutils.align(s_patch, fx, fy)
