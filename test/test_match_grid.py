
def match_grid(temp, im, gridsize):
    tw, th = get_bounds(temp, gridsize)
    blank = np.zeros_like(temp)
    for tx in range(tw):
        for ty in range(th):
            temp_patch = get_patch(temp, tx, ty, gridsize)
            im_patch = get_patch(im, tx, ty, gridsize)
            temp_patch_fft = fft.get_fft(temp_patch)
            im_patch_fft = fft.get_fft(im_patch)
            dx, dy = imagealign.phase_correlation(temp_patch_fft, im_patch_fft)
            x, y = get_patch_loc(tx, ty, gridsize)
            x += dx
            y += dy
            blank = merge_patch(blank, im_patch, x, y)
    return blank
