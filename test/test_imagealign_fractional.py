import numpy as np
import sys, os, cv2
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from alignment import imagealign, fft, imutils, patches, interp
# test: fractional alignment
# Take a patch of the original image
# Shift it fractionally using linear interp
# Try and match it back to original image

w, h = 45, 45
im = np.random.rand(w, h)
gap = np.ones((h, 1), np.float) # gap between images for display

grid_size = 15
tw, th = patches.get_bounds(im, grid_size)
tx, ty = 0, 1 # patch to use

step = 0.2
upsampling = 0
possibleshifts = np.arange(-1, 1, step)

def region(xmin, xmax, ymin, ymax):
    yslice = slice(np.clip(ymin, 0, None),np.clip(ymax, None, h))
    xslice = slice(np.clip(xmin, 0, None),np.clip(xmax, None, w))
    return yslice, xslice

sample = im.copy()
for i in range(upsampling):
    sample = cv2.pyrUp(sample)
cv2.imshow("Template", sample)
im_fft = fft.get_fft(im, upsampling)

for xs in possibleshifts:
    for ys in possibleshifts:
        xshift, yshift = patches.get_patch_loc(im, tx, ty, grid_size, overlap=2)
        print(xshift, yshift)
        # Generate shifted patch
        patch = patches.get_patch(im, tx, ty, grid_size, overlap=2)
        s_patch = patch #interp.upsampling(patch, xs, ys, 5)
        cv2.imshow("Patch", s_patch)
        sfft = fft.get_fft(s_patch, upsampling)
        dx = im_fft.shape[0] - sfft.shape[0]
        dy = im_fft.shape[1] - sfft.shape[1]
        pad_fft = np.pad(sfft, ((0,dx), (0, dy)), "constant", constant_values=(0))

        x, y = imagealign.phase_correlation(im_fft, pad_fft, upsampling)

        # Translate patch back to spot
        ix, iy = int(x), int(y)
        fx, fy = x-ix, y-iy
        print("hat: %f, %f" % (x,y))
        print("actual: %f, %f" % (xs, ys))
        print("Error: %f" % ((x-xs)**2+(y-ys)**2))
        cv2.waitKey(0)
        a_patch = imutils.align(s_patch, fx, fy)
        """
        sidebyside1 = np.hstack((im, gap, shifted_im))
        cv2.imshow("Original, shifted", sidebyside1)
        if chr(cv2.waitKey(0) & 0xFF) == "q":
            break

        shifted_im[region(xs, xs+w, ys, ys+h)] = \
            im[region(-xs, -xs+w, -ys, -ys+h)]
        crop_original = imutils.crop_offset(im, x, y)
        crop_shifted = imutils.crop_offset(shifted_im, x, y)
        crop_aligned = imutils.crop_offset(imaled, x, y)

        error = np.mean((crop_original-crop_aligned)**2)
        print("Error: %f" % error)
        seperator = np.ones(
            (imutils.crop_offset(imaled, x, y).shape[0], 1), np.float)
        sidebyside2 = np.hstack((
            crop_original, seperator,
            crop_shifted, seperator,
            crop_aligned))
        cv2.imshow("Original, shifted, aligned", sidebyside2)
        if chr(cv2.waitKey(0) & 0xFF) == "q":
            break
        """
