import numpy as np
import sys, os, cv2
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from alignment import imutils, interp

# test: linear interp

w, h = 10, 10
im = np.random.rand(w, h)
cv2.imshow("Original", im)
cv2.waitKey(0)
gap = np.ones((h, 1), np.float)

step = 0.2
possibleshifts = np.arange(-0.5, 0.5, step)

for xs in possibleshifts:
    for ys in possibleshifts:
        #sim = interp.linear(im, xs, ys)
        sim = interp.lanczos(im, ys, xs)
        #sim = imutils.align(sim, ys/2, xs/2)
        cv2.imshow("Shifted", sim)
        if chr(cv2.waitKey(0) & 0xFF) == "q":
            break
