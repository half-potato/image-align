import cv2, math
from . import imagealign, imutils
import numpy as np

def fft(row):
    r = np.multiply(row.flatten(), np.hamming(row.size))
    return np.fft.fft(row)

def alignRow(row_template_fft, row_fft):
    a = np.multiply(row_template_fft, np.conj(row_fft))
    R = a / np.linalg.norm(a)
    r = np.fft.ifft(R)
    x = r.argmax()
    if x > r.size / 2:
        x -= r.size
    return x

def alignRows(images):
    mean_im = np.mean(images, axis=0)
    ffts = [fft(i) for i in mean_im]

    # Align images
    shift = []
    aligned = []
    sum_error = 0
    for im in images:
        # Get shift using phase correlation
        shifts = []
        shifted = im.copy()
        for i, v in enumerate(im):
            row_fft = fft(v)
            shift = alignRow(ffts[i], row_fft)
            shifted[i, :] = np.roll(im[i, :], shift)
            shifts.append(shift)
        aligned.append(shifted)
        #cv2.imshow("HI", np.hstack((im, shifted))*0.0008)
        #if chr(cv2.waitKey(0) & 0xFF) == "q":
            #break

    # Crop to max
    shift = np.array(shift)
    max_pos = np.max(shift)
    max_neg = -np.max(shift)
    cropped = []
    def crop(im):
        return imutils.crop_offset(imutils.crop_offset(im, max_pos, 0), max_neg, 0)
    for im in aligned:
        im = crop(im)
        cropped.append(im)
    return cropped, crop
