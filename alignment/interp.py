import numpy as np
import sys, os, cv2, math
from . import imutils

def upsampling(im, x, y, upsampling):
    ups = im.copy()
    for i in range(upsampling):
        ups = cv2.pyrUp(ups)
    dx = imutils.round_n(x*2**upsampling)
    dy = imutils.round_n(y*2**upsampling)
    cropped = imutils.crop_offset(ups, dx, dy)
    for i in range(upsampling):
        cropped = cv2.pyrDown(cropped)
    return cv2.resize(cropped, im.shape)

def linear(im, x, y):
    ix, iy = imutils.ceil(x), imutils.ceil(y)
    xshift = imutils.align(im*x, ix, 0)
    neg_xshift = imutils.align(-im*x, -ix, 0)
    im_x = neg_xshift + im + xshift
    yshift = imutils.align(im_x*y, 0, iy)
    neg_yshift = imutils.align(-im_x*y, 0, -iy)
    im_xy = neg_yshift + im_x + yshift
    return im_xy

def sinc(x):
    return math.sin(math.pi*x) / (math.pi*x)

def lanczos_fn(x, a):
    if -a < x and x < a:
        return sinc(x)*sinc(x/a)
    else:
        return 0

def lanczos_window(a, xshift, yshift):
    print(a, xshift, yshift)
    x = [lanczos_fn(i+xshift, a) for i in range(a)]
    print(x)
    l_x = np.repeat(np.expand_dims(x, 0), a, axis=0)
    y = [lanczos_fn(i+yshift, a) for i in range(a)]
    l_y = np.repeat(np.expand_dims(y, 0), a, axis=0)
    return np.multiply(l_x, l_y.T)

def lanczos(im, x, y):
    lw = lanczos_window(3, x, y)
    lw = lw / np.sum(lw)
    print(np.sum(lw))
    return cv2.filter2D(im, -1, lw, borderType=cv2.BORDER_CONSTANT)
