import numpy as np
import mayavi.mlab

def bin(layers, layer_height=1):
    # Layers = array of images of layers
    im_shape = layers[0].shape
    height = layer_height*len(layers)
    grid = np.zeros((im_shape[0], im_shape[1], height))
    for i, layer in enumerate(layers):
        z_ind = int(np.floor(i*layer_height))
        grid[:,:,z_ind] += layer
        grid[:,:,z_ind] /= (np.ones_like(grid[:,:,z_ind]) * 2)
    return grid

def meshgrid_like(array):
    indexes = []
    for i in range(len(array.shape)):
        indexes.append(np.linspace(0, array.shape[i], array.shape[i], endpoint=False, dtype=int))
    return np.meshgrid(*indexes)

def point_indicies(array):
    indexes = []
    for i in range(len(array.shape)):
        indexes.append(np.linspace(0, array.shape[i], array.shape[i], endpoint=False, dtype=int))

def disp(grid, cutoff):
    xx, yy, zz = np.where(grid > cutoff)
    #mayavi.mlab.points3d(*ind, grid.flatten(), mode="cube", scale_factor=1)
    cols = grid[xx, yy, zz]
    print(cols)
    node = mayavi.mlab.points3d(xx, yy, zz, cols, scale_mode="none", mode="cube", scale_factor=1)
    #node.glyph.color_mode = "color_by_scalar"
    #node.mlab_source.dataset.point_data.scalars = cols
    mayavi.mlab.show()
