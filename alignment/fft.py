import cv2, math
import numpy as np

def extend_windowing(data, filter_function):
    """
    Performs an in-place windowing on N-dimensional spatial-domain data.
    This is done to mitigate boundary effects in the FFT.
    Taken from: https://stackoverflow.com/questions/27345861/extending-1d-function-across-3-dimensions-for-data-windowing

    Parameters
    ----------
    data : ndarray
           Input data to be windowed, modified in place.
    filter_function : 1D window generation function
           Function should accept one argument: the window length.
           Example: scipy.signal.hamming
    """
    dat = data.copy()
    for axis, axis_size in enumerate(data.shape):
        # set up shape for numpy broadcasting
        filter_shape = [1, ] * data.ndim
        filter_shape[axis] = axis_size
        window = filter_function(axis_size).reshape(filter_shape)
        # scale the window intensities to maintain image intensity
        dat *= np.power(window, (1.0/data.ndim))
    return dat


# Upsampling is the number of iterations of pyramid upsampling
def get_fft(im, upsampling=0, windower=np.hamming):
    for i in range(upsampling):
        im = cv2.pyrUp(im)
    if windower:
        im = extend_windowing(im, windower)
    return np.fft.fft2(im)
