import cv2, math
import numpy as np
from scipy.fftpack import fftn, ifftn
from scipy.signal import convolve2d
from . import fft, imutils

"""
def patch_phase_correlation(template_fft, patch_fft):
    filt = np.conj(patch_fft)
    conv = convolve2d(template_fft, filt)
    a = template_fft * 
    R = a / np.absolute(a)
    r = np.real(np.fft.ifft2(R))
"""

def phase_correlation2(template_fft, im):
    r = (ifftn(template_fft*ifftn(im))).real
    y, x = np.unravel_index(r.argmax(), r.shape)
    if y > r.shape[0]/2:
        y -= r.shape[0]
    if x > r.shape[0]/2:
        x -= r.shape[0]
    return x, y

def phase_correlation(template_fft, im_fft, downsampling=0):
    a = template_fft * np.ma.conjugate(im_fft)
    R = a / np.absolute(a)
    r = np.real(np.fft.ifft2(R))
    y, x = np.unravel_index(r.argmax(), r.shape)
    if y > r.shape[0]/2:
        y -= r.shape[0]
    if x > r.shape[0]/2:
        x -= r.shape[0]
    x = float(x) / (2**downsampling)
    y = float(y) / (2**downsampling)
    return x, y

# Non functional. Do not use. Based on Foroosh 2002
def subpixel(r):
    xm, ym = np.unravel_index(r.argmax(), r.shape) # Max peak
    m1 = 1 # +/- one so make it a var I guess

    # Find x
    xs = (xm + m1) % r.shape[0]
    dx1 = r[xs, ym]/(r[xs,ym] + r[xm,ym])
    dx2 = r[xs, ym]/(r[xs,ym] - r[xm,ym])
    dx = dx1 if np.sign(dx1)==np.sign(xs-xm) else dx2

    # Find y
    ys = (ym + m1) % r.shape[1]
    dy1 = r[xm, ys]/(r[xm,ys] + r[xm,ym])
    dy2 = r[xm, ys]/(r[xm,ys] - r[xm,ym])
    dy = dy1 if np.sign(dy1)==np.sign(ys-ym) else dy2

    return dy, dx

def cross_correlation(template, im, downsampling=0):
    r = cv2.matchTemplate(im, template, cv2.TM_CCORR)
    y, x = np.unravel_index(r.argmax(), r.shape)
    if y > r.shape[0]/2:
        y -= r.shape[0]
    if x > r.shape[0]/2:
        x -= r.shape[0]
    x = float(x) / (2**downsampling)
    y = float(y) / (2**downsampling)
    return x, y

def get_shift(template_fft, im_fft, upsampling=0):
    return phase_correlation(template_fft, im_fft, upsampling)

def alignImages(images, upsampling=0):
    # Create template
    mean_im = np.mean(images, axis=0)
    template = images[0]
    temp_fft = fft.get_fft(template, upsampling)

    # Align images
    shift = []
    aligned = []
    print("Aligning")
    for im in images:
        # Get shift using phase correlation
        x, y = get_shift(temp_fft, fft.get_fft(im, upsampling), upsampling)
        shift.append([x, y])
        im_a = imutils.align(im, x, y)

        # Crop images to offset
        temp_c = imutils.crop_offset(template, x, y)
        im_c = imutils.crop_offset(im, x, y)
        im_a_c = imutils.crop_offset(im_a, x, y)

        # Calculate error
        #prev_error = np.mean((temp_c - im_c)**2)
        #error = np.mean((temp_c - im_a_c)**2)
        #cv2.imshow("E", (temp_c - im_a_c)**2 * 1000)
        #if chr(cv2.waitKey(0) & 0xFF) == "q":
            #break
        #print("MSE prev = %f, MSE = %f" % (math.sqrt(prev_error), math.sqrt(error)))

        # Add original vs new based on error
        #if error > prev_error:
            #aligned.append(im)
        #else:
            #aligned.append(im_a)
        aligned.append(im_a)

    def crop_fn(im):
        return imutils.crop_shifts(im, shift)
    # Crop to max
    cropped = []
    for im in aligned:
        im = crop_fn(im)
        cropped.append(im)

    return cropped, crop_fn
