import cv2, math
import numpy as np

# Rounds up for pos/neg numbers
def ceil(num):
    return int(np.sign(num) * math.ceil(abs(num)))

# Rounds up/down for pos/neg numbers
def floor(num):
    return int(np.sign(num) * math.floor(abs(num)))

def round_n(num):
    return int(np.sign(num) * round(abs(num)))

# Crops the image so the offset is removed
def crop_offset(im, x, y):
    x = ceil(x)
    y = ceil(y)
    if x >= 0:
        startx = x
        endx = im.shape[1]
    else:
        startx = 0
        endx = im.shape[1] + x

    if y >= 0:
        starty = y
        endy = im.shape[0]
    else:
        starty = 0
        endy = im.shape[0] + y

    return im[starty:endy, startx:endx]

def crop_shifts(im, shifts):
    shift = np.array(shifts)
    max_pos_x = int(math.ceil(np.max(shift[:,0])))
    max_neg_x = int(math.ceil(-np.max(-shift[:,0])))
    max_pos_y = int(math.ceil(np.max(shift[:,1])))
    max_neg_y = int(math.ceil(-np.max(-shift[:,1])))
    return crop_offset(crop_offset(im, max_pos_x, max_pos_y),
                       max_neg_x, max_neg_y)

def d_norm(im):
    return cv2.normalize(im, None, 255,0, cv2.NORM_MINMAX, cv2.CV_8UC1)

# x and y are fractional. This linearly interpolates a fractional shift
def interp(patch, x, y, upsampling=0):
    pad = np.lib.pad(patch, ((1,1),(1,1)), "constant", constant_values=(0))
    for i in range(upsampling):
        pad = cv2.pyrUp(pad)
    center = (2**upsampling*patch.shape[0]/2 + x, 2**upsampling*patch.shape[1]/2 + y)
    shape = (2**upsampling*patch.shape[0], 2**upsampling*patch.shape[1])
    newpatch = cv2.getRectSubPix(np.float32(pad), shape, center)
    for i in range(upsampling):
        newpatch = cv2.pyrDown(newpatch)
    return newpatch

def align(im, x, y):
    im_x = np.roll(im, int(x), 1)
    im_xy = np.roll(im_x, int(y), 0)

    # Align fractional component
    fx, fy = x - int(x), y - int(y)
    if abs(fx) > 0.01 or abs(fy) > 0.01:
        im_xy = interp(im_xy, fx, fy, 0)

    return im_xy
