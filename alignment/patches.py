import cv2, math
import numpy as np
from . import imutils, imagealign, fft

def get_patch(im, ix, iy, grid_size):
    boundx, boundy = get_bounds(im, grid_size)
    if not (0 <= ix and ix < boundx):
        raise AssertionError("X out of bounds")
    if not (0 <= iy and iy < boundy):
        raise AssertionError("Y out of bounds")
    bx = np.clip(ix*grid_size, 0, im.shape[0])
    tx = np.clip(ix*grid_size+grid_size, 0, im.shape[0])

    by = np.clip(iy*grid_size, 0, im.shape[1])
    ty = np.clip(iy*grid_size+grid_size, 0, im.shape[1])
    """
    # Don't pad screws with matching
    padded = np.lib.pad(im, ((overlap,overlap), (overlap,overlap)), "minimum")
    return padded[by:ty, bx:tx]
    """
    return im[by:ty, bx:tx]

# Gives center
def get_patch_loc(im, ix, iy, grid_size):
    bx = np.clip(ix*grid_size, 0, im.shape[0])
    tx = np.clip(ix*grid_size+grid_size, 0, im.shape[0])

    by = np.clip(iy*grid_size, 0, im.shape[1])
    ty = np.clip(iy*grid_size+grid_size, 0, im.shape[1])
    return (bx+tx)/2., (by+ty)/2.

def get_bounds(im, grid_size):
    return int(math.ceil(im.shape[0] / float(grid_size))), int(math.ceil(im.shape[1] / float(grid_size)))

# Modifies in place
def merge_patch(im, patch, x, y):
    print("Merge patch")
    print(im.shape)
    print(patch.shape)
    print(x, y)
    """
    Parameters
    ----------
    im: Image to merge patch into
    patch: Image to merge into main image
    x, y: center of patch
    """
    # if negative then patch extends past zero
    half_patch_width = int(math.ceil(patch.shape[1]/2))
    half_patch_height = int(math.ceil(patch.shape[0]/2))
    topx = int(x - half_patch_width)
    topy = int(y - half_patch_height)
    negx = int(np.clip(topx, None, 0))
    negy = int(np.clip(topy, None, 0))
    # if negative then patch extends out of bounds
    posx = int(np.clip(im.shape[1] - half_patch_width - x-1, None, 0))
    posy = int(np.clip(im.shape[0] - half_patch_height - y-1, None, 0))

    topx = np.clip(topx, 0, im.shape[1]-patch.shape[1])
    topy = np.clip(topy, 0, im.shape[0]-patch.shape[0])

    # Get part of patch that is within image
    pat = patch[-negy:patch.shape[0]+posy, -negx:patch.shape[1]+posx]
    print(pat.shape)
    # Place within zero image
    zero = np.zeros_like(im)
    print("topy, topx, negx, negy, posx, posy")
    print(topy, topx, negx, negy, posx, posy)
    zero[topy:topy+pat.shape[0], topx:topx+pat.shape[1]] = pat

    # Calculate the overlap and deal with it seperately
    overlap = np.greater(np.multiply(zero, im), 0).astype(int) # overlap mask

    # Blur the patch into image
    # Get the overlap in each direction from the patch
    xpos_overlap = np.sum(overlap[topy:topy+pat.shape[0], topx+half_patch_height:], axis=0)
    xneg_overlap = np.sum(overlap[topy:topy+pat.shape[0], :topx+half_patch_height], axis=0)
    ypos_overlap = np.sum(overlap[topy+half_patch_height:, topx:topx+pat.shape[1]], axis=1)
    yneg_overlap = np.sum(overlap[:topy+half_patch_height, topx:topx+pat.shape[1]], axis=1)
    xpos_overlap = np.max(xpos_overlap) if xpos_overlap.size > 0 else 0
    xneg_overlap = np.max(xneg_overlap) if xneg_overlap.size > 0 else 0
    ypos_overlap = np.max(ypos_overlap) if ypos_overlap.size > 0 else 0
    yneg_overlap = np.max(yneg_overlap) if yneg_overlap.size > 0 else 0

    print(xpos_overlap, xneg_overlap, ypos_overlap, yneg_overlap)

    blur = np.zeros_like(im)
    blur[topy:topy+pat.shape[0], topx:topx+pat.shape[1]] = np.ones_like(pat)
    #blur -= overlap
    # Create blur for each direction
    if xpos_overlap > 0:
        xpos_linspace_blur = [np.linspace(1, 0, (xpos_overlap))]
        xpos_im_blur = np.repeat(xpos_linspace_blur, pat.shape[0], axis=0)
        xpos_linspace_horz = np.linspace(
            topx+pat.shape[1]-(xpos_overlap),
            topx+pat.shape[1],
            xpos_overlap, endpoint=False)
        xpos_linspace_vert = np.linspace(topy, topy+pat.shape[1],
                                        pat.shape[1], endpoint=False)
        xpos_j, xpos_i = np.meshgrid(xpos_linspace_horz.astype(int), xpos_linspace_vert.astype(int))
        blur[xpos_i, xpos_j] = xpos_im_blur

    if xneg_overlap > 0:
        xneg_linspace_blur = [np.linspace(0, 1, xneg_overlap)]
        xneg_im_blur = np.repeat(xneg_linspace_blur, pat.shape[0], axis=0)
        xneg_linspace_horz = np.linspace(
            topx,
            topx+xneg_overlap,
            xneg_overlap, endpoint=False)
        xneg_linspace_vert = np.linspace(topy, topy+pat.shape[1],
                                        pat.shape[1], endpoint=False)
        xneg_j, xneg_i = np.meshgrid(xneg_linspace_horz.astype(int), xneg_linspace_vert.astype(int))
        blur[xneg_i, xneg_j] = xneg_im_blur

    if ypos_overlap > 0:
        ypos_linspace_blur = [np.linspace(0, 1, (ypos_overlap))]
        ypos_im_blur = np.repeat(np.transpose(ypos_linspace_blur), pat.shape[1], axis=1)
        ypos_linspace_vert = np.linspace(
            topy,
            topy+ypos_overlap,
            ypos_overlap, endpoint=False)
        ypos_linspace_horz = np.linspace(topx, topx+pat.shape[0],
                                        pat.shape[0], endpoint=False)
        ypos_j, ypos_i = np.meshgrid(ypos_linspace_horz.astype(int), ypos_linspace_vert.astype(int))
        ypos_blur = np.zeros_like(im)
        ypos_blur[ypos_i, ypos_j] += ypos_im_blur
        botcorners_mask = np.greater(blur*ypos_blur, 0).astype(float) # mask with just the corners
        botcorners = blur*ypos_blur*botcorners_mask # just the average at the corners
        blur[ypos_i, ypos_j] = botcorners[ypos_i, ypos_j]

    if yneg_overlap > 0:
        yneg_linspace_blur = [np.linspace(1, 0, (yneg_overlap))]
        yneg_im_blur = np.repeat(np.transpose(yneg_linspace_blur), pat.shape[1], axis=1)
        yneg_linspace_vert = np.linspace(
            topy+pat.shape[0]-(yneg_overlap),
            topy+pat.shape[0],
            yneg_overlap, endpoint=False)
        yneg_linspace_horz = np.linspace(topx, topx+pat.shape[0],
                                        pat.shape[0], endpoint=False)
        yneg_j, yneg_i = np.meshgrid(yneg_linspace_horz.astype(int), yneg_linspace_vert.astype(int))
        yneg_blur = np.zeros_like(im)
        yneg_blur[yneg_i, yneg_j] += yneg_im_blur
        botcorners_mask = np.greater(blur*yneg_blur, 0).astype(float) # mask with just the corners
        botcorners = blur*yneg_blur*botcorners_mask # just the average at the corners
        blur[yneg_i, yneg_j] = botcorners[yneg_i, yneg_j]

    cv2.imshow("Blur", blur)

    blurred_patch = blur*zero
    merged = blurred_patch + im*np.abs(np.ones_like(blur)-blur)
    cv2.imshow("Merged", merged)
    return merged, blur

def get_containing_patch(im, x, y, grid_size):
    tx = imutils.floor(float(x) / grid_size)
    ty = imutils.floor(float(y) / grid_size)
    print(tx, ty)
    return get_patch(im, tx, ty, grid_size)

def match_grid(temp, im, gridsize):
    print("Match grid")
    print(temp.shape)
    cv2.imshow("Template", imutils.d_norm(im))
    tw, th = get_bounds(temp, gridsize)
    print(tw, th)
    blank = np.zeros_like(temp)
    blur_sum = np.zeros_like(temp)
    for tx in range(tw):
        for ty in range(th):
            temp_patch = get_patch(temp, tx, ty, gridsize)
            im_patch = get_patch(im, tx, ty, gridsize)
            temp_patch_fft = fft.get_fft(temp_patch)
            im_patch_fft = fft.get_fft(im_patch)
            dx, dy = imagealign.phase_correlation(temp_patch_fft, im_patch_fft)
            x, y = get_patch_loc(im, tx, ty, gridsize)
            x += dx
            y += dy
            blank, blur = merge_patch(blank, im_patch, x, y)
            blur_sum += blur
            #cv2.imshow("Blank", blank)
    blank /= blur_sum
    # Fill in the blanks with the template
    holes = np.where(blank==0)
    blank[holes] = temp[holes]
    return blank
