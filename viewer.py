import cv2, os, math, rowalign, imagealign
import numpy as np

# Get list of images for each dataset
def file_list(dirname):
    for d in os.listdir(dirname):
        p1 = os.path.join(dirname, d)
        if os.path.isdir(p1):
            for dataset_name in os.listdir(p1):
                p2 = os.path.join(p1, dataset_name)
                if os.path.isdir(p2) and dataset_name != "raw":
                    l = os.listdir(p2)
                    l.sort()
                    dataset = []
                    for f in l:
                        p3 = os.path.join(p2, f)
                        if os.path.isfile(p3):
                            dataset.append(p3)
                    yield dataset, dataset_name

im_brightness = 0.0008

for dataset, dataset_name in file_list("data"):
    ims = [cv2.imread(filename, -1).astype(float) for filename in dataset]

    for im in ims:
        cv2.imshow("HI", im*im_brightness)
        if chr(cv2.waitKey(0) & 0xFF) == "q":
            break
