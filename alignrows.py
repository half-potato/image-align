import cv2, os, math
import numpy as np
from alignment import rowalign, imagealign

# Get list of images for each dataset
def file_list(dirname):
    for d in os.listdir(dirname):
        p1 = os.path.join(dirname, d)
        if os.path.isdir(p1):
            for dataset_name in os.listdir(p1):
                p2 = os.path.join(p1, dataset_name)
                if os.path.isdir(p2) and dataset_name != "raw":
                    l = os.listdir(p2)
                    l.sort()
                    dataset = []
                    for f in l:
                        p3 = os.path.join(p2, f)
                        if os.path.isfile(p3):
                            dataset.append(p3)
                    yield dataset, dataset_name

im_brightness = 0.0008

for dataset, dataset_name in file_list("data"):
    print(dataset_name)
    if not "PKC" in dataset_name:
        continue
    print("Loading")
    ims = [cv2.imread(filename, -1).astype(float) for filename in dataset]
    print("Done")
    layers = []
    for i in range(10):
        layers.append(ims[i*10:i*10+10])

    for layer in layers:
        mean_im = np.mean(layer, axis=0)
        aligned, crop_fn = rowalign.alignRows(layer)
        #"""
        # Display new means side by side
        mean_aligned = np.mean(aligned, axis=0)
        mean_cropped = crop_fn(mean_im)
        layer0_cropped = crop_fn(layer[0])
        sidebyside = np.hstack((mean_cropped*im_brightness, mean_aligned*im_brightness, layer0_cropped*im_brightness))
        cv2.imshow("HI", sidebyside)
        if chr(cv2.waitKey(0) & 0xFF) == "q":
            break
