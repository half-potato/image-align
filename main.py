import cv2, os, math
import alignment
from alignment import rowalign, imagealign, patches
import numpy as np

im_brightness = 0.0008

for dataset, dataset_name in alignment.file_list("data"):
    print(dataset_name)
    if not "PKC" in dataset_name:
        continue
    print("Loading")
    ims = [cv2.imread(filename, -1).astype(float) for filename in dataset]
    print("Done")
    layers = []
    for i in range(10):
        layers.append(ims[i*10:i*10+10])

    for layer in layers:
        mean_im = np.mean(layer, axis=0)
        if "PKC" in dataset_name:
            layer, crop_fn1 = rowalign.alignRows(layer)
        layer, crop_fn2 = imagealign.alignImages(layer)
        matched = layer
        """
        for gridsize in range(layer[0].shape[0]//2, 40, -80):
            for i, im in enumerate(matched):
                m = patches.match_grid(layer[0], im, gridsize)
                matched[i] = m
                cv2.imshow("Patch", m)
        """
        #"""
        # Display new means side by side
        mean_aligned = np.mean(layer, axis=0)
        mean_cropped = crop_fn1(crop_fn2(mean_im))
        sidebyside = np.hstack((mean_cropped*im_brightness, mean_aligned*im_brightness, matched[-1]*im_brightness))
        cv2.imshow("HI", sidebyside)
        if chr(cv2.waitKey(0) & 0xFF) == "q":
            break
