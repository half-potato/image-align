if [ ! -d "data"]; then
  echo "Data directory not found. Did you extract all datasets to a directory named data?"
fi
cd data
for dataset in *
do
    cd $dataset
    mkdir raw
    mv *.tif raw/
    for i in raw/*.tif
    do
        mkdir $(basename $i .tif)
        convert $i $(basename $i .tif)/image.png
    done
    for i in *
    do
        cd $i
        rename 's/\d+/sprintf("%03d",$&)/e' image-*
        cd ..
    done
    cd ..
done
cd ..
