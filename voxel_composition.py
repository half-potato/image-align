import cv2, os, math
import alignment
from alignment import rowalign, imagealign, patches, voxels, imutils, fft
import numpy as np

im_brightness = 0.0008

for dataset, dataset_name in alignment.file_list("data"):
    print(dataset_name)
    #if not "PKC" in dataset_name:
    #    continue
    print("Loading")
    ims = [cv2.imread(filename, -1).astype(float) for filename in dataset]
    print("Done")
    mean_im = np.mean(ims, axis=0)
    if "PKC" in dataset_name:
        print("Aligning rows")
        ims, crop_fn1 = rowalign.alignRows(ims)
    #ims, crop_fn2 = imagealign.alignImages(ims)

    layers = []
    shifts = []
    print("Performing alignment")
    # Align each layer within itself
    for i in range(10):
        print("Aligning layer %i" % i)
        # Layer is every 10 images
        layer = ims[i*10:i*10+10]
        # Alignment template
        temp = fft.get_fft(layer[0])
        aligned = []
        # Align images
        for j in layer:
            x, y = imagealign.phase_correlation(temp, fft.get_fft(j))
            shifts.append([x, y])
            aligned.append(imutils.align(j, x, y))
        # Mean over layer is used
        im = np.mean(aligned, axis=0)
        layers.append(im)
    # Crop
    for i in range(len(layers)):
        layers[i] = imutils.crop_shifts(layers[i], shifts)
    # Display
    print("Displaying layers")
    for l in layers:
        cv2.imshow("Layer", imutils.d_norm(l))
        cv2.waitKey(100)

    layers, crop_fn3 = imagealign.alignImages(layers)

    vgrid = voxels.bin(layers)
    voxels.disp(vgrid, np.mean(layers)+np.std(layers)/16)
